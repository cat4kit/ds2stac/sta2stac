# SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for sta2stac."""

import pytest  # noqa: F401
