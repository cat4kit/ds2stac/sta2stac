..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0

.. _examples:


======================
Examples of use
======================
In this section, we provide a few examples of use of ``STA2STAC``. We listed them all in below:


.. toctree::

   STA2STAC_usage.ipynb
