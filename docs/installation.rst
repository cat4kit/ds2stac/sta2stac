.. SPDX-FileCopyrightText: 2023 KIT - Karlsruher Institut für Technologie
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _installation:

Installation
============

.. warning::

   This page has been automatically generated as has not yet been reviewed by the
   authors of sta2stac!

To install the `sta2stac` package, we recommend that
you install it from PyPi via::

    pip install sta2stac

Or install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac.git

The latter should however only be done if you want to access the development
versions.

.. _the source code repository on Gitlab: https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.
