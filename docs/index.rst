..
   SPDX-FileCopyrightText: 2023 Karlsruher Institut für Technologie

   SPDX-License-Identifier: CC-BY-4.0


..
   sta2stac documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

=============
STA2STAC
=============

**A Python Ecosystem for Harvesting Time Series data information from SensorthingsAPI (STA) and Cultivating STAC-Metadata.**


.. image:: https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/badges/main/pipeline.svg
   :target: https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/-/pipelines?page=1&scope=all&ref=main
.. image:: https://img.shields.io/pypi/v/sta2stac.svg
   :target: https://pypi.python.org/pypi/sta2stac/
.. image:: https://readthedocs.org/projects/sta2stac/badge/?version=latest
   :target: https://sta2stac.readthedocs.io/en/latest/
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. image:: https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/badges/main/coverage.svg
   :target: https://codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac/-/graphs/main/charts
.. image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac
   :target: https://api.reuse.software/info/codebase.helmholtz.cloud/cat4kit/ds2stac/sta2stac


Overview:
=================

The STA2STAC package serves as a bridge between the SensorThings API(STA) and the SpatioTemporal Asset Catalog (STAC) standard, improving the process of making STA time-series data more Findable, Accessible, Interoperable, and Reusable (FAIR).

STAC, as outlined on `SpatioTemporal Asset Catalog (STAC) <https://stacspec.org/en>`_, is a standardized framework that effectively delineates and organizes spatiotemporal assets utilizing a cohesive structure. The STA2STAC package is specifically developed to streamline the process of harvesting time series data from the `SensorThings API (STA) <https://www.ogc.org/standard/sensorthings/>`_, which is a widely-used approach for handling IoT sensor data. It then transforms this information into metadata that adheres to the STAC standard.


Installation
=================
You can find installation instructions in the [Installation](installation) section.

.. toctree::
   :maxdepth: 2

   installation

Tutorials
=========================
A comprehensive tutorial designed to enhance your understanding of the functionality of this project.

.. toctree::
   :maxdepth: 2

   tutorials/index

Examples
=========================
A bunch of examples is provided in this section.

.. toctree::
   :maxdepth: 2

   examples/index


API Reference
=========================
This API reference is auto-generated from the Python docstrings. The table of contents on the left is organized by module.

.. toctree::
   :maxdepth: 2

   api


Contribution
=========================


.. toctree::
   :maxdepth: 2

   contributing
